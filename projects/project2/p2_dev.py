"""MATH96012 Project 2
Jonathan Vincent
01201950
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as op
from m1 import lrmodel as lr #assumes that p2_dev.f90 has been compiled with:
from sklearn.neural_network import MLPClassifier
#f2py -c p2_dev.f90 -m m1
# May also use scipy, scikit-learn, and time modules as needed

def read_data(tsize=15000):
    """Read in image and label data from data.csv.
    The full image data is stored in a 784 x 20000 matrix, X
    and the corresponding labels are stored in a 20000 element array, y.
    The final 20000-tsize images and labels are stored in X_test and y_test, respectively.
    X,y,X_test, and y_test are all returned by the function.
    You are not required to use this function.
    """
    print("Reading data...") #may take 1-2 minutes
    Data=np.loadtxt('data.csv',delimiter=',')
    Data =Data.T
    X,y = Data[:-1,:]/255.,Data[-1,:].astype(int) #rescale the image, convert the labels to integers between 0 and M-1)
    Data = None

    # Extract testing data
    X_test = X[:,tsize:]
    y_test = y[tsize:]
    print("processed dataset")
    return X,y,X_test,y_test
#----------------------------

def clr_test(X,y,X_test,y_test,bnd=1.0,l=0.0,d=15000):
    """Train CLR model with input images and labels (i.e. use data in X and y),
     then compute and return testing error in test_error
    using X_test, y_test. The fitting parameters obtained
    via training should be returned in the 1-d array, fvec_f
    X: training image data, should be 784 x d with 1<=d<=15000
    y: training image labels, should contain d elements
    X_test,y_test: should be set as in read_data above
    bnd: Constraint parameter for optimization problem
    lambda: l2-penalty parameter for optimization problem
    input: tuple, set if and as needed
    """
    n = X.shape[0]
    y = y%2
    fvec = np.random.randn(n+1)*0.1  #initial fitting parameters

    fvec = np.clip(fvec,-bnd,bnd)
    lr.lr_y = y
    lr.lr_x = X
    lr.lr_lambda = l
    args = (d,n)
    bounds = [(-bnd,bnd),]*(n)
    bounds += [(-np.inf,np.inf)]
    bounds = tuple(bounds)

    #Minimising cost function with respect to weights and bias
    output = op.minimize(lr.clrmodel,fvec,args=args,method='L-BFGS-B',bounds=bounds,jac=True)

    #Outputing weights and bias
    weights = output.x[:-1]
    b = output.x[-1]
    fvec_f = output.x
    print("lambda = ", lr.lr_lambda)
    print("Cost = ",output.fun)

    #Calculate Training error
    z2 = np.matmul(X[:,:d].T,weights) + b
    a1 = 1/(1+np.exp(z2))
    decision = 1-np.around(a1)
    truth = np.logical_not(np.logical_xor(decision,y[:d]))
    trainingerror = 1-sum(truth)/d
    print("training error = ",trainingerror)

    #Testing remaining data
    testsize = len(y_test)
    y_test = y_test%2
    z2 = np.matmul(X_test.T,weights) + b
    a1 = 1/(1+np.exp(z2))
    decision = 1-np.around(a1)
    truth = np.logical_not(np.logical_xor(decision,y_test))
    test_error = 1-sum(truth)/testsize

    print("testing error = ",test_error)

    return fvec_f,test_error,trainingerror
#--------------------------------------------

def mlr_test(X,y,X_test,y_test,m=2,bnd=1.0,l=0.0,test=False,d=5000):
    """Train MLR model with input images and labels (i.e. use data in X and y),
     then compute and return testing error (in test_error)
    using X_test, y_test. The fitting parameters obtained via training should
    be returned in the 1-d array, fvec_f
    X: training image data, should be 784 x d with 1<=d<=15000
    y: training image labels, should contain d elements
    X_test,y_test: should be set as in read_data above
    m: number of classes
    bnd: Constraint parameter for optimization problem
    lambda: l2-penalty parameter for optimization problem
    input: tuple, set if and as needed
    """
    n = X.shape[0]
    y = y%m
    y_test = y_test%m
    fvec = np.random.randn((m-1)*(n+1))*0.1 #initial fitting parameters

    fvec = np.clip(fvec,-bnd,bnd)
    lr.lr_y = y
    lr.lr_x = X
    lr.lr_lambda = l

    args = (n,d,m)
    bounds = [(-bnd,bnd),]*((m-1)*(n))
    bbounds = [(-np.inf,np.inf),]*(m-1)
    bounds += bbounds
    bounds = tuple(bounds)

    output = op.minimize(lr.mlrmodel,fvec,args=args,method='L-BFGS-B',jac=True)

    #Outputing weights and bias
    weights = output.x[:-1]
    b = output.x[-1]
    fvec_f = output.x

    test_error,trainingerror = 0,0

    #Calculate Training error
    if test==True:
        z2 = np.matmul(X[:,:d].T,weights) + b
        a1 = 1/(1+np.exp(z2))
        decision = 1-np.around(a1)
        truth = np.logical_not(np.logical_xor(decision,y[:d]))
        trainingerror = 1-sum(truth)/d

        #Testing remaining data
        testsize = len(y_test)
        y_test = y_test%2
        z2 = np.matmul(X_test.T,weights) + b
        a1 = 1/(1+np.exp(z2))
        decision = 1-np.around(a1)
        truth = np.logical_not(np.logical_xor(decision,y_test))
        test_error = 1-sum(truth)/testsize
    return fvec_f,test_error,trainingerror
#--------------------------------------------

def lr_compare(X,y,X_test,y_test):
    """ Analyze performance of MLR and neural network models
    on image classification problem
    Add input variables and modify return statement as needed.
    Should be called from name==main section below
    """



    y, y_test = y%2, y_test%2

    plt.figure(1)
    plt.title('MLP Classifier')
    plt.xlabel('Training Data Size')
    plt.ylabel('Test Error')
    plt.plot(xdata,mlptesterror)
    plt.show()


    maxls = 31
    fvec_f = [0]*maxls
    test_error = [0]*maxls
    trainingerror = [0]*maxls
    xdata = []
    for i in range(maxls):
        l = 0.1*i
        xdata.append(l)
        fvec_f[i],test_error[i],trainingerror[i] = clr_test(X,y,X_test,y_test,l=l,d=15000)

    plt.figure(2)
    plt.title('Test Error as a function of lambda')
    plt.xlabel('Lambda')
    plt.ylabel('Test Error')
    plt.plot(xdata,test_error)
    plt.show()

    plt.figure(3)
    plt.title('Training Error as a function of lambda')
    plt.xlabel('Lambda')
    plt.ylabel('Training Error')
    plt.plot(xdata,trainingerror)
    plt.show()



    return None
#--------------------------------------------

def display_image(X):
    """Displays image corresponding to input array of image data"""
    n2 = X.size
    n = np.sqrt(n2).astype(int) #Input array X is assumed to correspond to an n x n image matrix, M
    M = X.reshape(n,n)
    plt.figure()
    plt.imshow(M)
    return None
#--------------------------------------------
#--------------------------------------------


if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    X,y,X_test,y_test = read_data()
    lr_compare(X,y,X_test,y_test)
    #fvec_f,test_error,trainingerror = clr_test(X,y,X_test,y_test)
    #output = lr_compare()
