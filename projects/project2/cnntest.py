import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier

def read_data(tsize=15000):
    """Read in image and label data from data.csv.
    The full image data is stored in a 784 x 20000 matrix, X
    and the corresponding labels are stored in a 20000 element array, y.
    The final 20000-tsize images and labels are stored in X_test and y_test, respectively.
    X,y,X_test, and y_test are all returned by the function.
    You are not required to use this function.
    """
    print("Reading data...") #may take 1-2 minutes
    Data=np.loadtxt('data.csv',delimiter=',')
    Data =Data.T
    X,y = Data[:-1,:]/255.,Data[-1,:].astype(int) #rescale the image, convert the labels to integers between 0 and M-1)
    Data = None

    # Extract testing data
    X_test = X[:,tsize:]
    y_test = y[tsize:]
    print("processed dataset")
    return X,y,X_test,y_test
#----------------------------

tsize=15000

X,y,X_test,y_test = read_data(15000)
y, y_test = y%2, y_test%2
X_train, y_train = X[:,:tsize], y[:tsize]

mlp = MLPClassifier(hidden_layer_sizes=(1,), max_iter=20, alpha=1e-4,
                    solver='sgd', tol=1e-4, random_state=1,
                    learning_rate_init=.1)

mlp.fit(X_train.T, y_train)
print("Training set score: %f" % mlp.score(X_train.T, y_train))
print("Test set score: %f" % mlp.score(X_test.T, y_test))
