!MATH96012 Project 2
!This module contains two module variables and three subroutines;
!two of these routines must be developed for this assignment.
!Module variables--
! lr_x: training images, typically n x d with n=784 and d<=15000
! lr_y: labels for training images, d-element array containing 0s and 1s
!   corresponding to images of even and odd integers, respectively.
!lr_lambda: l2-penalty parameter, should be set to be >=0.
!Module routines---
! data_init: allocate lr_x and lr_y using input variables n and d. May be used if/as needed.
! clrmodel: compute cost function and gradient using classical logistic
!   regression model (CLR) with lr_x, lr_y, and
!   fitting parameters provided as input
! mlrmodel: compute cost function and gradient using MLR model with m classes
!   and with lr_x, lr_y, and fitting parameters provided as input

module lrmodel
  implicit none
  real(kind=8), allocatable, dimension(:,:) :: lr_x
  integer, allocatable, dimension(:) :: lr_y
  real(kind=8) :: lr_lambda !penalty parameter

contains

!---allocate lr_x and lr_y deallocating first if needed (used by p2_main)--
! ---Use if needed---
subroutine data_init(n,d)
  implicit none
  integer, intent(in) :: n,d
  if (allocated(lr_x)) deallocate(lr_x)
  if (allocated(lr_y)) deallocate(lr_y)
  allocate(lr_x(n,d),lr_y(d))
end subroutine data_init


!Compute cost function and its gradient for CLR model
!for d images (in lr_x) and d labels (in lr_y) along with the
!fitting parameters provided as input in fvec.
!The weight vector, w, corresponds to fvec(1:n) and
!the bias, b, is stored in fvec(n+1)
!Similarly, the elements of dc/dw should be stored in cgrad(1:n)
!and dc/db should be stored in cgrad(n+1)
!Note: lr_x and lr_y must be allocated and set before calling this subroutine.
subroutine clrmodel(fvec,n,d,c,cgrad)
  implicit none
  integer, intent(in) :: n,d !training data sizes
  real(kind=8), dimension(n+1), intent(in) :: fvec !fitting parameters
  real(kind=8), intent(out) :: c !cost
  real(kind=8), dimension(n+1), intent(out) :: cgrad !gradient of cost
  !Declare other variables as needed
  real(kind=8), dimension(n) :: x,w
  real(kind=8) :: b ,zsum
  real(kind=8), dimension(2) :: z, a!real variables
  integer :: j1, k1, y

  w = fvec(1:n)
  b = fvec(n+1)
  z(1) = 0
  c = 0
  c = lr_lambda*sum(w*w)
  cgrad = 2*lr_lambda*w

  do k1 = 1,d
    x = lr_x(:,k1)
    y = lr_y(k1)
    z(2) = dot_product(x,w) + b
    zsum = sum(exp(z))
    a(1) = exp(z(1))/zsum
    a(2) = exp(z(2))/zsum

    if (y == 1) then
        c = c - log(a(2))
        cgrad(n+1) = cgrad(n+1) - (1-a(2))
        do j1 = 1,n
          cgrad(j1)= cgrad(j1) - (1-a(2))*x(j1)
        end do
      else
        c = c - log(1-a(2))
        cgrad(n+1) = cgrad(n+1) + a(2)
        do j1 = 1,n
          cgrad(j1)= cgrad(j1) + a(2)*x(j1)
        end do
      end if
  end do


end subroutine clrmodel


!!Compute cost function and its gradient for MLR model
!for d images (in lr_x) and d labels (in lr_y) along with the
!fitting parameters provided as input in fvec. The labels are integers
! between 0 and m-1.
!fvec contains the elements of the weight matrix w and the bias vector, b
! Code has been provided below to "unpack" fvec
!The elements of dc/dw and dc/db should be stored in cgrad
!and should be "packed" in the same order that fvec was unpacked.
!Note: lr_x and lr_y must be allocated and set before calling this subroutine.
subroutine mlrmodel(fvec,n,d,m,c,cgrad)
  implicit none
  integer, intent(in) :: n,d,m !training data sizes and number of classes
  real(kind=8), dimension((m-1)*(n+1)), intent(in) :: fvec !fitting parameters
  real(kind=8), intent(out) :: c !cost
  real(kind=8), dimension((m-1)*(n+1)), intent(out) :: cgrad !gradient of cost
  integer :: i1,j1,k1, y
  real(kind=8), dimension(m-1,n) :: w
  real(kind=8), dimension(m-1) :: b
  real(kind=8), dimension(m) :: a, z
  real(kind=8), dimension(n) :: x
  !Declare other variables as needed

  !unpack fitting parameters (use if needed)
  do i1=1,n
    j1 = (i1-1)*(m-1)+1
    w(:,i1) = fvec(j1:j1+m-2) !weight matrix
  end do

  b = fvec((m-1)*n+1:(m-1)*(n+1)) !bias vector
  c = lr_lambda*sum(w*w)

  do k1=1,d
    x = lr_x(:,k1)
    y = lr_y(k1)
    z(1) = 0
    z(2:) = matmul(w,x)+ b
    a = exp(z)/(sum(exp(z)))
    c = c - log(a(y+1)+(1.0d-12))

    do i1=1,m-1
      if (i1 == y) then
        do j1 = 1,n
          cgrad((i1-1)*n+j1)= cgrad((i1-1)*n+j1) - (1-a(i1+1))*x(j1)
        end do

        cgrad((m-1)*n+i1) = cgrad((m-1)*n+i1) - (1-a(i1+1))
      else
        do j1 = 1,n
          cgrad((i1-1)*n+j1)= cgrad((i1-1)*n+j1) + a(i1+1)*x(j1)
        end do

        cgrad((m-1)*n+i1) = cgrad((m-1)*n+i1) + a(i1+1)
      end if
    end do
  end do

end subroutine mlrmodel



end module lrmodel
