
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as op
from m1 import lrmodel as lr

def read_data(tsize=15000):
    """Read in image and label data from data.csv.
    The full image data is stored in a 784 x 20000 matrix, X
    and the corresponding labels are stored in a 20000 element array, y.
    The final 20000-tsize images and labels are stored in X_test and y_test, respectively.
    X,y,X_test, and y_test are all returned by the function.
    You are not required to use this function.
    """
    print("Reading data...") #may take 1-2 minutes
    Data=np.loadtxt('data.csv',delimiter=',')
    Data =Data.T
    X,y = Data[:-1,:]/255.,Data[-1,:].astype(int) #rescale the image, convert the labels to integers between 0 and M-1)
    Data = None

    # Extract testing data
    X_test = X[:,tsize:]
    y_test = y[tsize:]
    print("processed dataset")
    return X,y,X_test,y_test
# ----------------------------

bnd = 1
lr.lr_lambda = 0
m=2

X,y,X_test,y_test = read_data()
n = X.shape[0]
y = y%m
y_test = y_test%m
fvec = np.random.randn((m-1)*(n+1))*0.1 #initial fitting parameters
fvec = np.clip(fvec,-bnd,bnd)
d = 1000
lr.lr_y = y
lr.lr_x = X

args = (d,n)
bounds = [(-bnd,bnd),]*(n)
bounds += [(-np.inf,np.inf)]
bounds = tuple(bounds)

output = op.minimize(lr.clrmodel,fvec,args=args,method='L-BFGS-B',jac=True,options={'disp': True})

args = (n,d,m)
bounds = [(-bnd,bnd),]*((m-1)*(n))
bbounds = [(-np.inf,np.inf),]*(m-1)
bounds += bbounds
bounds = tuple(bounds)

output = op.minimize(lr.mlrmodel,fvec,args=args,method='L-BFGS-B',jac=True,options={'disp': True})
#output = op.minimize(cost,fvec,args=args,method='L-BFGS-B',options={'disp': True})
