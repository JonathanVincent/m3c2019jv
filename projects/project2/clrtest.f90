
program clrtest
  implicit none
  integer :: n,d, i1 !training data sizes
  real(kind=8), allocatable, dimension(:):: fvec !fitting parameters
  real(kind=8), allocatable, dimension(:) :: cgrad !gradient of cost
  !Declare other variables as needed
  real(kind=8) :: sum_i, b, zsum, c
  real(kind=8), dimension(769) :: z, a, w  !real variables
  n=768
  d = 5000
  allocate(fvec(n))
  do i1 = 1,n
    fvec(i1) = 0.5
  end do
  fvec(n+1) = 0.5
  print* , n
  !Add code to compute c and cgrad
  w = fvec(1:n)
  b = fvec(n+1)
  do i1 = 2,n
      z(i1) = b
  end do
  z(1) = 1

  zsum = sum(z)
    do i1 = 1,n
      a(i1) = exp(z(i1))/zsum
      print *, a(i1)
    end do

    !do i1 = 1,d
      !c = c + lr_y(i1)*log(a(i1)) + (1-lr_y(i1))*log(1-a(i1))
    !end do

end program clrtest
