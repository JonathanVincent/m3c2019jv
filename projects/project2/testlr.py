
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as op
from m1 import lrmodel as lr

def read_data(tsize=15000):
    """Read in image and label data from data.csv.
    The full image data is stored in a 784 x 20000 matrix, X
    and the corresponding labels are stored in a 20000 element array, y.
    The final 20000-tsize images and labels are stored in X_test and y_test, respectively.
    X,y,X_test, and y_test are all returned by the function.
    You are not required to use this function.
    """
    print("Reading data...") #may take 1-2 minutes
    Data=np.loadtxt('data.csv',delimiter=',')
    Data =Data.T
    X,y = Data[:-1,:]/255.,Data[-1,:].astype(int) #rescale the image, convert the labels to integers between 0 and M-1)
    Data = None

    # Extract testing data
    X_test = X[:,tsize:]
    y_test = y[tsize:]
    print("processed dataset")
    return X,y,X_test,y_test
# ----------------------------


bnd = 1
lr.lr_lambda = 0.5

X,y,X_test,y_test = read_data()
n = X.shape[0]
y = y%2
fvec = np.random.randn(n+1)*0.1  #initial fitting parameters
fvec = np.clip(fvec,-bnd,bnd)
d = 15000
lr.lr_y = y
lr.lr_x = X
args = (d,n)
bounds = [(-bnd,bnd),]*(n)
bounds += [(-np.inf,np.inf)]
bounds = tuple(bounds)

#Minimising cost function with respect to weights and bias
output = op.minimize(lr.clrmodel,fvec,args=args,method='L-BFGS-B',bounds=bounds,jac=True)

#Outputing weights and bias
weights = output.x[:-1]
b = output.x[-1]
print("lambda = ", lr.lr_lambda)
print("Cost = ",output.fun)
print("Bias = ",b)

#Calculate Training error
z2 = np.matmul(X[:,:d].T,weights) + b
a1 = 1/(1+np.exp(z2))
decision = 1-np.around(a1)
truth = np.logical_not(np.logical_xor(decision,y[:d]))
trainingerror = 1-sum(truth)/d
print("training error = ",trainingerror)

#Testing remaining data
testsize = len(y_test)
y_test = y_test%2
z2 = np.matmul(X_test.T,weights) + b
a1 = 1/(1+np.exp(z2))
decision = 1-np.around(a1)
truth = np.logical_not(np.logical_xor(decision,y_test))
testingerror = 1-sum(truth)/testsize
print("testing error = ",testingerror)
