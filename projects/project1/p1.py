"""MATH96012 2019 Project 1
Jonathan Vincent
01201950
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.spatial as spat
import time
#--------------------------------

def simulate1(N=64,L=8,s0=0.2,r0=1,A=0.2,Nt=100):
    """Part1: Simulate bacterial colony dynamics
    Input:
    N: number of particles
    L: length of side of square domain
    s0: speed of particles
    r0: particles within distance r0 of particle i influence direction of motion
    of particle i
    A: amplitude of noise
    dt: time step
    Nt: number of time steps

    Output:
    X,Y: position of all N particles at Nt+1 times
    alpha: alignment parameter at Nt+1 times

    Do not modify input or return statement without instructor's permission.

    Add brief description of approach to problem here:
    
    At each timestep, a distance matrix 'dist' is computed using the scipy module,
    with the entry dist_ij being equal to the Euclidean distance between the
    coordinates (x_i,y_i) and (x_j,y_j).
    
    This NxN distance matrix is then converted into an adjacency matrix 'adj', by
    setting all values <= r_0 equal to 1 and all values > r_0 equal to 0. A value
    of 1 indicates the pair of particles are interacting.
    A degree vector 'deg' is created by summing along each column of 'adj', this
    denotes how many neighbouring particles n_j are affecting each particle j.
    
    The updated value of theta (at t+1) can now be computed, by adding the
    neighbour component adj*exp(i*theta(t)) and the random component
    A*deg*exp(i*R), then taking the argument.
    
    From this X and Y can be updated using the formulas given, and then taken
    modulo L to ensure that the boundary conditions are met.
    
    Alpha is computed using the formula given.
    
    All of the above steps use matrices and vectors to minimise computation time.
    
    
    """
    #Set initial condition
    phi_init = np.random.rand(N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(N))
    Xinit,Yinit = r_init*np.cos(phi_init),r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2

    theta_init = np.random.rand(N)*(2*np.pi) #initial directions of motion
    #---------------------
    
    X = np.zeros((N,Nt+1))
    Y = np.zeros((N,Nt+1))
    theta = np.zeros((N,Nt+1))
    alpha= np.zeros(Nt+1)
    X[:,0] = Xinit
    Y[:,0] = Yinit
    theta[:,0] = theta_init
    alpha[0] = 1/N * abs(sum(np.exp(1j*theta[:,0])))
    R = np.random.rand(N,Nt)*(2*np.pi)  #Pre-computing all random variates
    
    for t in range(Nt):
        M = np.transpose((X[:,t],Y[:,t]))
        dist = spat.distance_matrix(M,M)  #Using the in-built Scipy function
        adj = dist <= r0                  #Building the adjacency matrix
        deg = np.sum(adj, axis=0)         #Computing degree of each particle
        #print(R)
        theta[:,t+1] = np.angle(adj.dot(np.exp(theta[:,t]*1j))
                       + A*deg*np.exp(1j*R[:,t]))
                       
        X[:,t+1] = (X[:,t] + s0*np.cos(theta[:,t+1])) % L
	#modulo L ensures boundary conditions are met
        Y[:,t+1] = (Y[:,t] + s0*np.sin(theta[:,t+1])) % L
        alpha[t+1] = 1/N * abs(sum(np.exp(1j*theta[:,t+1])))

    return X,Y,alpha


def analyze(NA=24,sample=80):
    
    """Part 2: Analysis
    NA is the number of A values to be tested
    sample is the number of samples to take per A value
    
    Takes 3-4 mins to run at default values. Lower the values to increase speed.
    """
    x = np.linspace(0.25,0.75,num=6)
    plt.figure(1)
    for A in x:
        X,Y,alpha32 = simulate1(A=A,Nt=200,L=4,N=32)
        plt.plot(alpha32,label=str(A))
    plt.title("Alignment Over Time")
    plt.xlabel("Number of Timesteps")
    plt.ylabel("Alpha")
    plt.legend()
    plt.show()
    
    
    #Calculates mean variance for both 16 and 32 particles
    meanvaralpha16 = np.zeros(NA+1)
    meanvaralpha32 = np.zeros(NA+1)
    x = np.linspace(0.2,0.8,num=NA+1)
    i=0
    #main loop
    for A in x:
        varalpha16 = np.zeros(sample)
        varalpha32 = np.zeros(sample)
        for j in range(sample):
            X,Y,alpha16 = simulate1(A=A,Nt=500,L=4,N=16)
            X,Y,alpha32 = simulate1(A=A,Nt=500,L=4,N=32)
            varalpha16[j] = np.var(alpha16[100:])
            varalpha32[j] = np.var(alpha32[100:])
        meanvaralpha16[i] = sum(varalpha16)/len(varalpha16)
        meanvaralpha32[i] = sum(varalpha32)/len(varalpha32)
        i+=1
        print("A = " + str(A) + " Complete")
    #creating figures
    plt.figure(2)
    plt.plot(x,meanvaralpha16,label="16 Particles")
    plt.plot(x,meanvaralpha32,label="32 Particles")
    plt.title("Alignment Variance")
    plt.xlabel("A")
    plt.ylabel("Mean Variance of Alpha")
    plt.legend()
    plt.show()
    Astar16 = x[np.argmax(meanvaralpha16)]
    Astar32 = x[np.argmax(meanvaralpha32)]
    print("A*16 = " + str(Astar16))
    print("A*32 = " + str(Astar32))
    
    
    #Computing y = 1-A/A*
    y=1-(x[:np.argmax(meanvaralpha16)-1]/Astar16)
    
    plt.figure(3)
    plt.semilogy(y,meanvaralpha16[:len(y)],label='Semilog Plot')
    plt.title("Alignment Variance Log Plot")
    plt.xlabel("1 - A/A*")
    plt.ylabel("Mean Variance of Alpha")
    plt.plot([y[0],y[-1]],[meanvaralpha16[0],meanvaralpha16[len(y)-1]],label='Linear Fit')
    plt.legend()
    grad = (meanvaralpha16[0]-meanvaralpha16[len(y)-1])/(y[0]-y[-1])
    #print(grad)
    #approximate gradient of semilog graph.
    plt.show()
    
    return grad



def simulate2(N=64,L=8,s0=0.2,r0=1,A=0.2,Nt=100):
    """Part 2: Simulation code for Part 2, add input variables and simulation
    code needed for simulations required by analyze
    """
    return simulate1(N,L,s0,r0,A,Nt)
    
def animation(Nt = 200,L=8,N=64,r0=1,A=0.3):
    #Animate particles moving with trail of 5 particles.
    X,Y,alpha = simulate1(A=A,N=N,Nt=Nt,L=L,r0=r0)
    plt.axis([0, L, 0, L])
    plt.title('Simulation with 5 Timestep Trail')
    for i in range(Nt-5):
        pts1 = plt.scatter(X[:,i],Y[:,i],alpha=0.05,c="b")
        pts2 = plt.scatter(X[:,i+1],Y[:,i+1],alpha=0.1,c="b")
        pts3 = plt.scatter(X[:,i+2],Y[:,i+2],alpha=0.4,c="b")
        pts4 = plt.scatter(X[:,i+3],Y[:,i+3],alpha=0.7,c="b")
        pts5 = plt.scatter(X[:,i+4],Y[:,i+4],alpha=1,c="b")
        plt.pause(0.005)
        plt.show()
        pts1.remove()
        pts2.remove()
        pts3.remove()
        pts4.remove()
        pts5.remove()
    plt.close()
    plt.figure(2)
    plt.plot(alpha)
    plt.show()
    


#--------------------------------------------------------------
if __name__ == '__main__':
    #The code here should call analyze and
    #generate the figures that you are submitting with your
    #discussion.
    output_a = analyze()
    #animation()
