"""MATH 96012 Project 3
Jonathan Vincent
01201950
Contains four functions:
    simulate2: Simulate bacterial dynamics over m trials. Return: all positions at final time
        and alpha at nt+1 times averaged across the m trials.
    performance: To be completed -- analyze and assess performance of python, fortran, and fortran+openmp simulation codes
    correlation: To be completed -- compute and analyze correlation function, C(tau)
    visualize: To be completed -- generate animation illustrating "non-trivial" particle dynamics
"""
import numpy as np
import matplotlib.pyplot as plt
from m1 import bmotion as bm #assumes that p3_dev.f90 has been compiled with: f2py --f90flags='-fopenmp' -c p3_dev.f90 -m m1 -lgomp
import scipy.spatial.distance as scd
import time
import matplotlib.animation as animation


def simulate2(M=10,N=64,L=8,s0=0.2,r0=1,A=0,Nt=100):
    """Simulate bacterial colony dynamics
    Input:
    M: Number of simulations
    N: number of particles
    L: length of side of square domain
    s0: speed of particles
    r0: particles within distance r0 of particle i influence direction of motion
    of particle i
    A: amplitude of noise
    Nt: number of time steps

    Output:
    X,Y: position of all N particles at Nt+1 times
    alpha: alignment parameter at Nt+1 times averaged across M simulation

    Do not modify input or return statement without instructor's permission.

    Add brief description of approach of differences from simulate1 here:
    This code carries out M simulations at a time with partial vectorization
    across the M samples.
    """
    #Set initial condition
    phi_init = np.random.rand(M,N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(M,N))
    Xinit,Yinit = r_init*np.cos(phi_init),r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2
    #---------------------

    #Initialize variables
    P = np.zeros((M,N,2)) #positions
    P[:,:,0],P[:,:,1] = Xinit,Yinit
    alpha = np.zeros((M,Nt+1)) #alignment parameter
    S = np.zeros((M,N),dtype=complex) #phases
    T = np.random.rand(M,N)*(2*np.pi) #direction of motion
    n = np.zeros((M,N)) #number of neighbors
    E = np.zeros((M,N,Nt+1),dtype=complex)
    d = np.zeros((M,N,N))
    dtemp = np.zeros((M,N*(N-1)//2))
    AexpR = np.random.rand(M,N,Nt)*(2*np.pi)
    AexpR = A*np.exp(1j*AexpR)

    r0sq = r0**2
    E[:,:,0] = np.exp(1j*T)

    #Time marching-----------
    for i in range(Nt):
        for j in range(M):
            dtemp[j,:] = scd.pdist(P[j,:,:],metric='sqeuclidean')

        dtemp2 = dtemp<=r0sq
        for j in range(M):
            d[j,:,:] = scd.squareform(dtemp2[j,:])
        n = d.sum(axis=2) + 1
        S = E[:,:,i] + n*AexpR[:,:,i]

        for j in range(M):
            S[j,:] += d[j,:,:].dot(E[j,:,i])

        T = np.angle(S)

        #Update X,Y
        P[:,:,0] = P[:,:,0] + s0*np.cos(T)
        P[:,:,1] = P[:,:,1] + s0*np.sin(T)

        #Enforce periodic boundary conditions
        P = P%L

        E[:,:,i+1] = np.exp(1j*T)
    #----------------------

    #Compute order parameter
    alpha = (1/(N*M))*np.sum(np.abs(E.sum(axis=1)),axis=0)

    return P[:,:,0],P[:,:,1],alpha


def simulate2viz(M=1,N=64,L=8,s0=0.2,r0=1,A=0,Nt=100):
    """The same as above but all X and Y values are returned for all nt
    This is used by the visualisation function
    This uses the above python code as a template because we will only be running
    one simulation to visualise, so efficiency (ie using fortran) is not as
    important as when running many.
    """
    #Set initial condition
    phi_init = np.random.rand(M,N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(M,N))
    Xinit,Yinit = r_init*np.cos(phi_init),r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2
    #---------------------

    #Initialize variables
    P = np.zeros((M,N,2)) #positions
    Phist = np.zeros((M,N,2,Nt)) #history of positions
    P[:,:,0],P[:,:,1] = Xinit,Yinit
    alpha = np.zeros((M,Nt+1)) #alignment parameter
    S = np.zeros((M,N),dtype=complex) #phases
    T = np.random.rand(M,N)*(2*np.pi) #direction of motion
    n = np.zeros((M,N)) #number of neighbors
    E = np.zeros((M,N,Nt+1),dtype=complex)
    d = np.zeros((M,N,N))
    dtemp = np.zeros((M,N*(N-1)//2))
    AexpR = np.random.rand(M,N,Nt)*(2*np.pi)
    AexpR = A*np.exp(1j*AexpR)

    r0sq = r0**2
    E[:,:,0] = np.exp(1j*T)

    #Time marching-----------
    for i in range(Nt):
        #Record Position
        Phist[:,:,:,i] = P[:,:,:]

        for j in range(M):
            dtemp[j,:] = scd.pdist(P[j,:,:],metric='sqeuclidean')

        dtemp2 = dtemp<=r0sq
        for j in range(M):
            d[j,:,:] = scd.squareform(dtemp2[j,:])
        n = d.sum(axis=2) + 1
        S = E[:,:,i] + n*AexpR[:,:,i]

        for j in range(M):
            S[j,:] += d[j,:,:].dot(E[j,:,i])

        T = np.angle(S)

        #Update X,Y
        P[:,:,0] = P[:,:,0] + s0*np.cos(T)
        P[:,:,1] = P[:,:,1] + s0*np.sin(T)


        #Enforce periodic boundary conditions
        P = P%L

        E[:,:,i+1] = np.exp(1j*T)
    #----------------------

    #Compute order parameter
    alpha = (1/(N*M))*np.sum(np.abs(E.sum(axis=1)),axis=0)

    return P[:,:,0],P[:,:,1],alpha,Phist


def performance(input_p,display=False):
    """Assess performance of simulate2, simulate2_f90, and simulate2_omp
    Modify the contents of the tuple, input, as needed
    When display is True, figures equivalent to those
    you are submitting should be displayed
    """

    if display == False:
        return

    interval = input_p

    n=100
    l=8
    s0=0.2
    r0=1
    a=0.64
    nt=50

    bm.bm_l = l
    bm.bm_s0 = s0
    bm.bm_r0 = r0
    bm.bm_a = a

    x = []
    pythontime = []
    f90time = []
    omptime1 = []
    omptime2 = []
    omptime4 = []

    for m in range(1,202,interval):
        print("m = ",m)
        x.append(m)
        t1 = time.time()#start$timer$1
        X,Y,alpha = simulate2(m,n,l,s0,r0,a,nt)
        t2 = time.time()#start$timer$1
        pythontime.append(t2-t1)

        t1 = time.time()#start$timer$1
        X,Y,alpha = bm.simulate2_f90(m,n,nt)
        t2 = time.time()#start$timer$1
        f90time.append(t2-t1)

        bm.numthreads = 1
        t1 = time.time()#start$timer$1
        X,Y,alpha = bm.simulate2_omp(m,n,nt)
        t2 = time.time()#start$timer$1
        omptime1.append(t2-t1)

        bm.numthreads = 2
        t1 = time.time()#start$timer$1
        X,Y,alpha = bm.simulate2_omp(m,n,nt)
        t2 = time.time()#start$timer$1
        omptime2.append(t2-t1)

        bm.numthreads = 4
        t1 = time.time()#start$timer$1
        X,Y,alpha = bm.simulate2_omp(m,n,nt)
        t2 = time.time()#start$timer$1
        omptime4.append(t2-t1)

    plt.figure(1)
    plt.plot(x,pythontime,label="simulate2 Python")
    plt.plot(x,f90time,label="simulate2_f90")
    plt.plot(x,omptime1,label="simulate2_omp 1 thread")
    plt.plot(x,omptime2,label="simulate2_omp 2 threads")
    plt.plot(x,omptime4,label="simulate2_omp 4 threads")
    plt.xlabel("M")
    plt.ylabel("Time (seconds)")
    plt.title("Time taken to run each routine with different M values")
    plt.legend()
    plt.show()


    rp = [omptime1[i]/pythontime[i] for i in range(len(pythontime))]
    rf90 = [omptime1[i]/f90time[i] for i in range(len(pythontime))]
    romp1 = [omptime1[i]/omptime1[i] for i in range(len(pythontime))]
    romp2 = [omptime1[i]/omptime2[i] for i in range(len(pythontime))]
    romp4 = [omptime1[i]/omptime4[i] for i in range(len(pythontime))]


    plt.figure(2)
    plt.plot(x,rp,label="simulate2 Python")
    plt.plot(x,rf90,label="simulate2_f90")
    plt.plot(x,romp1, label="simulate2_omp 1 thread")
    plt.plot(x,romp2, label="simulate2_omp 2 threads")
    plt.plot(x,romp4, label="simulate2_omp 4 threads")
    plt.legend()
    plt.xlabel("M")
    plt.ylabel("Speed-up")
    plt.title("Speed up compared to simulate_omp with 1 thread")
    plt.show()

    m = 100
    x = []
    pythontime = []
    f90time = []
    omptime1 = []
    omptime2 = []
    omptime4 = []

    for n in range(1,202,interval):
        print("n = ",n)
        x.append(n)
        t1 = time.time()#start$timer$1
        X,Y,alpha = simulate2(m,n,l,s0,r0,a,nt)
        t2 = time.time()#start$timer$1
        pythontime.append(t2-t1)

        t1 = time.time()#start$timer$1
        X,Y,alpha = bm.simulate2_f90(m,n,nt)
        t2 = time.time()#start$timer$1
        f90time.append(t2-t1)

        bm.numthreads = 1
        t1 = time.time()#start$timer$1
        X,Y,alpha = bm.simulate2_omp(m,n,nt)
        t2 = time.time()#start$timer$1
        omptime1.append(t2-t1)

        bm.numthreads = 2
        t1 = time.time()#start$timer$1
        X,Y,alpha = bm.simulate2_omp(m,n,nt)
        t2 = time.time()#start$timer$1
        omptime2.append(t2-t1)

        bm.numthreads = 4
        t1 = time.time()#start$timer$1
        X,Y,alpha = bm.simulate2_omp(m,n,nt)
        t2 = time.time()#start$timer$1
        omptime4.append(t2-t1)

    plt.figure(3)
    plt.plot(x,pythontime,label="simulate2 Python")
    plt.plot(x,f90time,label="simulate2_f90")
    plt.plot(x,omptime1,label="simulate2_omp 1 thread")
    plt.plot(x,omptime2,label="simulate2_omp 2 threads")
    plt.plot(x,omptime4,label="simulate2_omp 4 threads")
    plt.xlabel("N")
    plt.ylabel("Time (seconds)")
    plt.title("Time taken to run each routine with different N values")
    plt.legend()
    plt.show()

    rp = [omptime1[i]/pythontime[i] for i in range(len(pythontime))]
    rf90 = [omptime1[i]/f90time[i] for i in range(len(pythontime))]
    romp1 = [omptime1[i]/omptime1[i] for i in range(len(pythontime))]
    romp2 = [omptime1[i]/omptime2[i] for i in range(len(pythontime))]
    romp4 = [omptime1[i]/omptime4[i] for i in range(len(pythontime))]

    plt.figure(4)
    plt.plot(x,rp,label="simulate2 Python")
    plt.plot(x,rf90,label="simulate2_f90")
    plt.plot(x,romp1, label="simulate2_omp 1 thread")
    plt.plot(x,romp2, label="simulate2_omp 2 threads")
    plt.plot(x,romp4, label="simulate2_omp 4 threads")
    plt.legend()
    plt.xlabel("N")
    plt.ylabel("Speed-up")
    plt.title("Speed up compared to simulate_omp with 1 thread")
    plt.legend()
    plt.show()


    return [rp,rf90,romp1,romp4] #Modify as needed

def correlation(input_c,display=False):
    """Compute and analyze temporal correlation function, C(tau)
    Modify the contents of the tuple, input, as needed
    When display is True, figures equivalent to those
    you are submitting should be displayed
    """
    trials = input_c

    n=400
    l=16
    s0=0.1
    r0=1
    a=0.625
    nt=881
    m = 1

    lower = 500
    upper = lower + 200
    upper2 = lower + 300

    div = 1/(upper-lower)
    c = [0]*trials
    c2 = [0]*trials
    alphas = [[]]*trials
    corrs = [[]]*trials
    corrs2 = [[]]*trials

    for j in range(trials):
        print("Trial =",j)
        X,Y,alpha = simulate2(m,n,l,s0,r0,a,nt)  #Using python function as m is small
        alphas[j] = alpha
        corr = []
        corr2 = []
        for tau in range(0,80):
            c[j] = 0
            c2[j] = 0
            for i in range(lower,upper+1):
                c[j] += div*alpha[i+tau]*alpha[i] - (div*alpha[i])**2
                c2[j] += div*alpha[i+tau]*alpha[i] - (div*alpha[i])**2
            for i in range(upper+1,upper2+1):
                c2[j] += div*alpha[i+tau]*alpha[i] - (div*alpha[i])**2
            corr.append(c[j])
            corr2.append(c2[j])
        corrs[j] = corr
        corrs2[j] = corr2

    corrmean = []
    corr2mean = []


    for tau in range(0, 80):
        corrsum = 0
        corr2sum = 0
        for j in range(trials):
            corrsum += corrs[j][tau]
            corr2sum += corrs2[j][tau]
        corrmean.append(corrsum/trials)
        corr2mean.append(corr2sum/trials)


    if display == True:

        plt.figure(6)
        plt.ylabel("C")
        plt.xlabel("Tau")
        plt.title("Correlation C as a function of Tau")
        for j in range(trials):
            plt.plot(corrs[j])
        plt.show()

        plt.figure(5)
        plt.ylabel("alpha")
        plt.xlabel("Time")
        plt.title("Alpha as a function of time")
        for j in range(min(trials,5)):
            plt.plot(alphas[j])
        plt.show()

        plt.figure(7)
        plt.ylabel("C")
        plt.xlabel("Tau")
        plt.title("Average Correlation")
        plt.plot(corrmean,label = "Mean Correlation b-a=200")
        coeffs = np.polyfit(range(len(corrmean)),corrmean,1)
        z = [coeffs[0]*i + coeffs[1] for i in range(len(corrmean))]
        plt.plot(range(len(corrmean)),z,label = "Linear Fit for Correlation b-a=200")

        plt.plot(corr2mean,label = "Mean Correlation b-a=300")
        coeffs2 = np.polyfit(range(len(corrmean)),corr2mean,1)
        z2 = [coeffs2[0]*i + coeffs2[1] for i in range(len(corrmean))]
        plt.plot(range(len(corrmean)),z2,label = "Linear Fit for Correlation b-a=300")

        plt.legend()
        plt.show()

    return [corrs, corrs2]


def updatefig(i):
    """Updates animation figure each time function is called
    """
    global Phist
    global line
    print("iteration=",i)
    line.set_xdata(Phist[0,:,0,i])
    line.set_ydata(Phist[0,:,1,i])
    return line,

def visualize(display=False):
    """This function produces an animation of the bacteria movement and then
    saves it to an mp4 file. Setting display = True will also show the animation
    in python. This will save the file to animation.mp4 in the current directory."""


    global Phist
    global line
    n=128
    l=8
    s0=0.15
    r0=1
    a=0.35
    nt=300
    m = 1

    # Running the simulation
    X,Y,alpha,Phist = simulate2viz(M=m,N=n,L=l,s0=s0,r0=r0,A=a,Nt=nt)

    #Set up initial figure
    fig, ax = plt.subplots()
    line, = ax.plot(Phist[0,:,0,0],Phist[0,:,1,0],'.')
    ax.set_xlim(0,8)
    ax.set_ylim(0,8)

    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

    # Create and save animation
    ani = animation.FuncAnimation(fig, updatefig, frames=nt,interval=100,repeat=False)
    plt.title("Animation of Bacterial Flow")
    ani.save('animation.mp4', writer=writer)

    if display == True:
        plt.show()

    return None


if __name__ == '__main__':

    # The interval between m values, select values between 20 and 100, the lower
    # the number the slower it will take.
    interval = 100
    # Performance function can take a few minutes to run with low interval
    output_p = performance(input_p=interval,display=True)

    # The number of trials to calculate the
    trials = 2
    # Correlation will analyse C(tau)
    output_c = correlation(input_c=trials,display=True)

    # Uncomment line below to produce an animation as an mp4 file
    visualize(display=True)
