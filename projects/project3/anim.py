import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from m1 import bmotion as bm #assumes that p3_dev.f90 has been compiled with: f2py --f90flags='-fopenmp' -c p3_dev.f90 -m m1 -lgomp
import scipy.spatial.distance as scd
import time


def simulate2viz(M=1,N=128,L=8,s0=0.2,r0=1,A=0.3,Nt=100):
    """The same as above but all X and Y values are returned
    """
    #Set initial condition
    phi_init = np.random.rand(M,N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(M,N))
    Xinit,Yinit = r_init*np.cos(phi_init),r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2
    #---------------------

    #Initialize variables
    P = np.zeros((M,N,2)) #positions
    Phist = np.zeros((M,N,2,Nt)) #history of positions
    P[:,:,0],P[:,:,1] = Xinit,Yinit
    alpha = np.zeros((M,Nt+1)) #alignment parameter
    S = np.zeros((M,N),dtype=complex) #phases
    T = np.random.rand(M,N)*(2*np.pi) #direction of motion
    n = np.zeros((M,N)) #number of neighbors
    E = np.zeros((M,N,Nt+1),dtype=complex)
    d = np.zeros((M,N,N))
    dtemp = np.zeros((M,N*(N-1)//2))
    AexpR = np.random.rand(M,N,Nt)*(2*np.pi)
    AexpR = A*np.exp(1j*AexpR)

    r0sq = r0**2
    E[:,:,0] = np.exp(1j*T)

    #Time marching-----------
    for i in range(Nt):
        #Record Position
        Phist[:,:,:,i] = P[:,:,:]

        for j in range(M):
            dtemp[j,:] = scd.pdist(P[j,:,:],metric='sqeuclidean')

        dtemp2 = dtemp<=r0sq
        for j in range(M):
            d[j,:,:] = scd.squareform(dtemp2[j,:])
        n = d.sum(axis=2) + 1
        S = E[:,:,i] + n*AexpR[:,:,i]

        for j in range(M):
            S[j,:] += d[j,:,:].dot(E[j,:,i])

        T = np.angle(S)

        #Update X,Y
        P[:,:,0] = P[:,:,0] + s0*np.cos(T)
        P[:,:,1] = P[:,:,1] + s0*np.sin(T)


        #Enforce periodic boundary conditions
        P = P%L

        E[:,:,i+1] = np.exp(1j*T)
    #----------------------

    #Compute order parameter
    alpha = (1/(N*M))*np.sum(np.abs(E.sum(axis=1)),axis=0)

    return P[:,:,0],P[:,:,1],alpha,Phist

X,Y,alpha,Phist = simulate2viz()

#Set up initial figure
fig, ax = plt.subplots()
line, = ax.plot(Phist[0,:,0,0],Phist[0,:,1,0],'.')
ax.set_xlim(0,8)
ax.set_ylim(0,8)


def updatefig(i):
    """Updates figure each time function is called
    and returns new figure 'axes'
    """
    global Phist
    print("iteration=",i)
    line.set_xdata(Phist[0,:,0,i])
    line.set_ydata(Phist[0,:,1,i])
    return line,

#updatefig is called 30 times, and each iteration of the image is
#stored as a frame in an animation
ani = animation.FuncAnimation(fig, updatefig, frames=100,interval=100,repeat=False)
plt.show()
