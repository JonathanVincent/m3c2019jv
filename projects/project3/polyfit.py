#polyfit

import numpy as np
import numpy.random as ra
import matplotlib.pyplot as plt
from m1 import bmotion as bm #assumes that p3_dev.f90 has been compiled with: f2py --f90flags='-fopenmp' -c p3_dev.f90 -m m1 -lgomp
import scipy.spatial.distance as scd
import time
import matplotlib.animation as animation


y = ra.random(100)
print(y)
y = [j*y[j] for j in range(len(y))]
x = [i for i in range(len(y))]


coeffs = np.polyfit(x,y,1)
z = [coeffs[0]*i + coeffs[1] for i in range(len(y))]


plt.plot(x,y)
plt.plot(x,z)
plt.show()
