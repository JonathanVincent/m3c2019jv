"""Final project, part 1"""
import numpy as np
import matplotlib.pyplot as plt
from m1 import flow as fl #assumes p41.f90 has been compiled with: f2py -c p41.f90 -m m1
import time
#Use f2py -c p41_dev.f90 -m m1
#python3 -m numpy.f2py --f90flags='-fopenmp' -c p41_dev.f90 -m m1 -lgomp


def jacobi(n,kmax=10000,tol=1.0e-8,s0=0.1,display=False):
    """ Solve liquid flow model equations with
        jacobi iteration.
        Input:
            n: number of grid points in r and theta
            kmax: max number of iterations
            tol: convergence test parameter
            s0: amplitude of cylinder deformation
            display: if True, plots showing the velocity field and boundary deformation
            are generated
        Output:
            w,deltaw: Final velocity field and |max change in w| each iteration
    """

    #-------------------------------------------
    #Set Numerical parameters and generate grid
    Del_t = 0.5*np.pi/(n+1)
    Del_r = 1.0/(n+1)
    Del_r2 = Del_r**2
    Del_t2 = Del_t**2
    r = np.linspace(0,1,n+2)
    t = np.linspace(0,np.pi/2,n+2) #theta
    tg,rg = np.meshgrid(t,r) # r-theta grid

    #Factors used in update equation (after dividing by gamma)
    rg2 = rg*rg
    fac = 0.5/(rg2*Del_t2 + Del_r2)
    facp = rg2*Del_t2*fac*(1+0.5*Del_r/rg) #alpha_p/gamma
    facm = rg2*Del_t2*fac*(1-0.5*Del_r/rg) #alpha_m/gamma
    fac2 = Del_r2*fac #beta/gamma
    RHS = fac*(rg2*Del_r2*Del_t2) #1/gamma

    #set initial condition/boundary deformation
    w0 = (1-rg**2)/4 #Exact solution when s0=0
    s_bc = s0*np.exp(-10.*((t-np.pi/2)**2))/Del_r
    fac_bc = s_bc/(1+s_bc)

    deltaw = []
    w = w0.copy()
    wnew = w0.copy()

    #Jacobi iteration
    for k in range(kmax):
        #Compute wnew
        wnew[1:-1,1:-1] = RHS[1:-1,1:-1] + w[2:,1:-1]*facp[1:-1,1:-1] + w[:-2,1:-1]*facm[1:-1,1:-1] + (w[1:-1,:-2] + w[1:-1,2:])*fac2[1:-1,1:-1] #Jacobi update

        #Apply boundary conditions
        wnew[:,0] = wnew[:,1] #theta=0
        wnew[:,-1] = wnew[:,-2] #theta=pi/2
        wnew[0,:] = wnew[1,:] #r=0
        wnew[-1,:] = wnew[-2,:]*fac_bc #r=1s

        #Compute delta_p
        deltaw += [np.max(np.abs(w-wnew))]
        w = wnew.copy()
        if k%1000==0: print("k,dwmax:",k,deltaw[k])
        #check for convergence
        if deltaw[k]<tol:
            print("Converged,k=%d,dw_max=%28.16f " %(k,deltaw[k]))
            break

    deltaw = deltaw[:k+1]

    if display:
        #plot final velocity field, difference from initial guess, and cylinder
        #surface
        plt.figure()
        plt.contour(t,r,w,50)
        plt.xlabel(r'$\theta$')
        plt.ylabel('r')
        plt.title('Final velocity field')

        plt.figure()
        plt.contour(t,r,np.abs(w-w0),50)
        plt.xlabel(r'$\theta$')
        plt.ylabel('r')
        plt.title(r'$|w - w_0|$')

        plt.figure()
        plt.polar(t,np.ones_like(t),'k--')
        plt.polar(t,np.ones_like(t)+s_bc*Del_r,'r-')
        plt.title('Deformed cylinder surface')

    return w,deltaw

def unpack(wflat):
    wsgi = [[]]*(n+2)
    for i in range(n+2):
        wsgi[i] = list(wflat[(n+2)*i:(i+1)*(n+2)])
    return wsgi



def performance():
    """Analyze performance of codes
    Add input/output variables as needed.
    """
    fl.fl_kmax = 10000
    fl.numthreads = 1
    n = 100
    r = np.linspace(0,1,n+2)
    t = np.linspace(0,np.pi/2,n+2) #theta

    fljacerror = []
    flsgierror = []
    x1 = []
    jactime = []
    sgitime = []
    sgi2time = []

    for i in range(10,101,5):
        n=i
        x1.append(n)
        print(i,"Jacobi:")
        wjac = fl.jacobi(n)
        fljacerror.append(fl.fl_deltaw[-1])

        print(i,"SGI 1 thread:")
        fl.numthreads = 1
        wflat = fl.sgisolve(n)
        flsgierror.append(fl.fl_deltaw[-1])

    x2 = []

    for i in range(10,261,50):
        n=i
        x2.append(n)
        print(i,"Jacobi:")
        t1 = time.time()
        wjac = fl.jacobi(n)
        t2 = time.time()
        jactime.append(t2-t1)

        print(i,"SGI 1 thread:")
        fl.numthreads = 1
        t1 = time.time()
        wflat = fl.sgisolve(n)
        t2 = time.time()
        sgitime.append(t2-t1)

        print(i,"SGI 2 threads:")
        fl.numthreads = 2
        t1 = time.time()
        wflat = fl.sgisolve(n)
        t2 = time.time()
        sgi2time.append(t2-t1)

    plt.figure(1)
    plt.semilogy(x1,fljacerror,label="Jacobi Error")
    plt.semilogy(x1,flsgierror,label="SGI Error")
    plt.legend()
    plt.title("Error after 10000 iterations")


    plt.figure(2)
    plt.semilogy(x1[:10],fljacerror[:10],label="Jacobi Error")
    plt.semilogy(x1[:10],flsgierror[:10],label="SGI Error")
    plt.legend()
    plt.title("Error after 10000 iterations")


    plt.figure(4)
    plt.plot(x2,jactime,label="Jacobi Time")
    plt.plot(x2,sgitime,label="SGI Time")
    plt.plot(x2,sgi2time,label="SGI 2 thread Time")
    plt.legend()
    plt.title("Time taken after 10000 iterations")
    plt.show()



    return None



if __name__=='__main__':
    #Add code below to call performance
    #and generate figures you are submitting in
    #your repo.
    input=()
    n=50
    performance()
