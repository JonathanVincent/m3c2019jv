! This is a main program which can be used with the bmotion module
! if and as you wish as you develop your codes.
! It reads simulation parameters from a text file, data.in, which must be created.
!
! The subroutine simulate2_f90 is called, and output is written to the text files,
! x.txt,y.txt, and alpha_ave.txt which can be read in Python using np.loadtxt (see below)
!
! You should not submit this code with your assignment.
! To compile: gfortran -fopenmp -O3 -o main.exe p41_dev.f90 p4_main.f90
program p4_main
  use flow
  implicit none
  integer,parameter :: n=200
  real(kind=8), dimension(n+2) :: fac,fac2,facp,facm,fac_bc
  real(kind=8), dimension((n+2)*(n+2)) :: y
  real(kind=8), dimension((n+2)*(n+2)) :: z
  real(kind=8), dimension(n+2) :: v,r,r2,t,RHS,s_bc
  real(kind=8), dimension((n+2)*(n+2)) :: b,x,e,enew,d,Ad,Md
  real(kind=8) :: del_t,del_r,del_t2,del_r2,kappa,mu
  integer :: i1,j1,k1,l1,m

  m = (n+2)**2

  !grid spacings------------
  del_t = 0.5d0*pi/dble(n+1)
  del_r = 1.d0/dble(n+1)

  del_r2 = del_r**2
  del_t2 = del_t**2

  y = 0.1d0

  do i1=1,n+2
      r(i1) = i1*del_r
  end do

  r2 = r**2
  fac = 0.5d0/(r2*del_t2 + del_r2)
  facp = r2*del_t2*fac*(1.d0+0.5d0*del_r/r) !alpha_p/gamma
  facm = r2*del_t2*fac*(1.d0-0.5d0*del_r/r) !alpha_m/gamma1.d0
  fac2 = del_r2 * fac !beta/gamma

  s_bc = fl_s0*exp(-10.d0*((t-pi/2.d0)**2))
  fac_bc = s_bc/(del_r+s_bc)

  !fac2 = 0.01d0 !doesnt work
  !facm = 0.0d0 !doesnt work
  !facp = 0.0d0 !doesnt work
  !fac_bc = 0.0d0


  !call mvec(n,fac,fac2,facp,facm,fac_bc,y,z)
  !print*,z
  !print*, sum(z)!-floor(sum(z))
  !print*,"mtvec"
  !call mtvec(n,fac,fac2,facp,facm,fac_bc,y,z)
  !print*,z
  !print*, sum(z)!-floor(sum(z))
  call jacobi(n,z)
  print*,z(2*n:2*n+3)
  call sgisolve(n,z)
  print*,z(2*n:2*n+3)

end program p4_main
