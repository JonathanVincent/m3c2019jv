!Final project part 1
!Module for flow simulations of liquid through tube
!This module contains a few module variables (see comments below)
!and four subroutines:
!jacobi: Uses jacobi iteration to compute solution
! to flow through tube
!sgisolve: To be completed. Use sgi method to
! compute flow through tube
!mvec: To be completed; matrix-vector multiplication z = Ay
!mtvec: To be completed; matrix-vector multiplication z = A^T y
module flow
    implicit none
    real(kind=8), parameter :: pi = acos(-1.d0)
    integer :: numthreads !number of threads used in parallel regions
    integer :: fl_kmax=10000 !max number of iterations
    real(kind=8) :: fl_tol=0.00000001d0 !convergence criterion
    real(kind=8), allocatable, dimension(:) :: fl_deltaw !|max change in w| each iteration
    real(kind=8) :: fl_s0=0.1d0 !deformation magnitude

contains
!-----------------------------------------------------
!Solve 2-d tube flow problem with Jacobi iteration
subroutine jacobi(n,w)
    !input  n: number of grid points (n+2 x n+2) grid
    !output w: final velocity field
    !Should also compute fl_deltaw(k): max(|w^k - w^k-1|)
    !A number of module variables can be set in advance.

    integer, intent(in) :: n
    real(kind=8), dimension(0:n+1,0:n+1), intent(out) :: w
    integer :: i1,j1,k1
    real(kind=8) :: del_r,del_t,del_r2,del_t2
    real(kind=8), dimension(0:n+1) :: s_bc,fac_bc
    real(kind=8), dimension(0:n+1,0:n+1) :: r,r2,t,RHS,w0,wnew,fac,fac2,facp,facm

    if (allocated(fl_deltaw)) then
      deallocate(fl_deltaw)
    end if
    allocate(fl_deltaw(fl_kmax))


    !grid--------------
    del_t = 0.5d0*pi/dble(n+1)
    del_r = 1.d0/dble(n+1)
    del_r2 = del_r**2
    del_t2 = del_t**2


    do i1=0,n+1
        r(i1,:) = i1*del_r
    end do

    do j1=0,n+1
        t(:,j1) = j1*del_t
    end do
    !-------------------

    !Update-equation factors------
    r2 = r**2
    fac = 0.5d0/(r2*del_t2 + del_r2)
    facp = r2*del_t2*fac*(1.d0+0.5d0*del_r/r) !alpha_p/gamma
    facm = r2*del_t2*fac*(1.d0-0.5d0*del_r/r) !alpha_m/gamma
    fac2 = del_r2 * fac !beta/gamma
    RHS = fac*(r2*del_r2*del_t2) !1/gamma
    !----------------------------

    !set initial condition/boundary deformation
    w0 = (1.d0-r2)/4.d0
    w = w0
    wnew = w0
    s_bc = fl_s0*exp(-10.d0*((t(0,:)-pi/2.d0)**2))/del_r
    fac_bc = s_bc/(1.d0+s_bc)

    !Jacobi iteration
    do k1=1,fl_kmax
        wnew(1:n,1:n) = RHS(1:n,1:n) + w(2:n+1,1:n)*facp(1:n,1:n) + w(0:n-1,1:n)*facm(1:n,1:n) + &
                                         (w(1:n,0:n-1) + w(1:n,2:n+1))*fac2(1:n,1:n)

        !Apply boundary conditions
        wnew(:,0) = wnew(:,1) !theta=0
        wnew(:,n+1) = wnew(:,n) !theta=pi/2
        wnew(0,:) = wnew(1,:) !r=0
        wnew(n+1,:) = wnew(n,:)*fac_bc !r=1s

        fl_deltaw(k1) = maxval(abs(wnew-w)) !compute relative error

        w=wnew    !update variable
        if (fl_deltaw(k1)<fl_tol) exit !check convergence criterion
        if (mod(k1,1000)==0) print *, k1,fl_deltaw(k1)
    end do

    print *, 'k,error=',k1,fl_deltaw(min(k1,fl_kmax))

end subroutine jacobi
!-----------------------------------------------------

!Solve 2-d tube flow problem with sgi method
subroutine sgisolve(n,w)
  !input  n: number of grid points (n+2 x n+2) grid
  !output w: final velocity field stored in a column vector
  !Should also compute fl_deltaw(k): max(|w^k - w^k-1|)
  !A number of module variables can be set in advance.
  integer, intent(in) :: n
  real(kind=8), dimension((n+2)*(n+2)), intent(out) :: w
  real(kind=8) :: del_t,del_r,del_t2,del_r2,kappa,mu,maxchange
  real(kind=8), dimension(n+2) :: fac,fac2,facp,facm,fac_bc,r,r2,t,RHS,s_bc
  real(kind=8), dimension((n+2)*(n+2)) :: b,e,enew,d,Ad,Md,v
  integer :: i1,j1,k1,l1,m
  !add other variables as needed
  m = (n+2)**2
  CALL OMP_SET_NUM_THREADS(numthreads)

  !grid spacings------------
  del_t = 0.5d0*pi/dble(n+1)
  del_r = 1.d0/dble(n+1)
  del_r2 = del_r**2
  del_t2 = del_t**2

  do i1=1,n+2
      r(i1) = (i1-1)*del_r
  end do

  do j1=1,n+2
      t(j1) = (j1-1)*del_t
  end do

  r2 = r**2
  fac = 0.5d0/(r2*del_t2 + del_r2)
  facp = r2*del_t2*fac*(1.d0+0.5d0*del_r/r) !alpha_p/gamma
  facm = r2*del_t2*fac*(1.d0-0.5d0*del_r/r) !alpha_m/gamma1.d0
  fac2 = del_r2 * fac !beta/gamma
  RHS = fac*(r2*del_r2*del_t2) !1/gamma

  s_bc = fl_s0*exp(-10.d0*((t-pi/2.d0)**2))
  fac_bc = s_bc/(del_r+s_bc)

  b = 0.0d0

  do i1=1,n
    k1 = 1+i1*(n+2)
    b(k1+1:k1+n) = -RHS(i1+1)
  end do

  call mvec(n,fac,fac2,facp,facm,fac_bc,b,v)

  call mtvec(n,fac,fac2,facp,facm,fac_bc,b,v)

  d = v
  w = 0.0d0
  e = v

  do l1=1,fl_kmax

    call mvec(n,fac,fac2,facp,facm,fac_bc,d,Ad)
    call mtvec(n,fac,fac2,facp,facm,fac_bc,Ad,Md)
    kappa = sum(e*e)/sum(d*Md)

    w = w + kappa*d
    maxchange = maxval(abs(kappa*d))
    fl_deltaw(l1) = maxchange
    enew = e - kappa*Md
    mu = sum(enew*enew)/sum(e*e)
    e = enew
    d = e + mu*d

    if (maxchange<fl_tol) exit !check convergence criterion
    if (mod(l1,1000)==0) print *, l1,maxchange

  end do

  print*,l1,maxchange

  !print*, "complete"

end subroutine sgisolve


!Compute matrix-vector multiplication, z = Ay
subroutine mvec(n,fac,fac2,facp,facm,fac_bc,y,z)
    !input n: grid is (n+2) x (n+2)
    ! fac,fac2,facp,facm,fac_bc: arrays that appear in
    !   discretized equations
    ! y: vector multipled by A
    !output z: result of multiplication Ay
    implicit none
    integer, intent(in) :: n
    real(kind=8), dimension(n+2), intent(in) :: fac,fac2,facp,facm,fac_bc
    real(kind=8), dimension((n+2)*(n+2)), intent(in) :: y
    real(kind=8), dimension((n+2)*(n+2)), intent(out) :: z
    integer :: i1,k1,j1,m,p

    p=n+2
    m = p**2
    !Inner BCs
    !r=0
    z(1:p) = -y(1:p) + y(p+1:2*p)

    !$OMP parallel do private(i1,k1,j1)

    do i1 = 1,n
      !theta = 0
      k1 = 1 + i1*p
      z(k1) = - y(k1) + y(k1+1)

      do j1 = 2,p-1
        k1 = i1*p + j1
        z(k1) = facm(i1+1)*y(k1-p) + fac2(i1+1)*(y(k1-1)+y(k1+1)) + facp(i1+1)*y(k1+p) - y(k1)
     end do

      !BCs
      !theta = pi/2
      k1 = (i1+1)*p
      z(k1) = y(k1-1) - y(k1)
    end do

    !$OMP end parallel do

    !Outer BCs
    !r=1s
    z(m+1-p:m) = -y(m+1-p:m) + fac_bc(1:p)*y(m+1-2*p:m-p)


end subroutine mvec


!Compute matrix-vector multiplication, z = A^T y
subroutine mtvec(n,fac,fac2,facp,facm,fac_bc,y,z)
    !input n: grid is (n+2) x (n+2)
    ! fac,fac2,facp,facm,fac_bc: arrays that appear in
    !   discretized equations
    ! y: vector multipled by A^T
    !output z: result of multiplication A^T y
    implicit none
    integer, intent(in) :: n
    real(kind=8), dimension(n+2), intent(in) :: fac,fac2,facp,facm,fac_bc
    real(kind=8), dimension((n+2)*(n+2)), intent(in) :: y
    real(kind=8), dimension((n+2)*(n+2)), intent(out) :: z
    integer :: i1,j1,k1,m,p
    !add other variables as needed
    p = n+2
    m = p**2

    !Inner BCs
    !r=0
    z(1) = - y(1)
    z(2:p-1) = -y(2:p-1) + facm(2)*y(2+p:2*p-1)
    z(p) = - y(p)
    !print*,z(1:n+2)

    !i = 1
    z(p+1) = y(1) - y(p+1) + fac2(2)*y(p+2)
    z(p+2) = y(2) + y(p+1) - y(p+2) + fac2(2)*y(p+3) + facm(3)*y(2*p + 2)
    z(p+3:2*p-2) = y(3:p-2) + fac2(2)*(y(p+2:2*p-3) + y(p+4:2*p-1)) - y(p+3:2*p-2) +&
    facm(3)*y(2*p+3:3*p-2)
    z(2*p-1) = y(p-1) + fac2(2)*y(2*p-2) - y(2*p-1) + y(2*p) + facm(3)*y(3*p-1)
    z(2*p) = y(p) + fac2(2)*y(2*p-1) - y(2*p)

    !$OMP parallel do private(i1,k1,j1)

    do i1 = 2,n-1
      k1 = i1*p + 1
      !print*,k1
      z(k1) = -y(k1) + fac2(i1+1)*y(k1+1)

      k1 = i1*p + 2
      z(k1) = y(k1-1)- y(k1) + fac2(i1+1)*y(k1+1) + facp(i1)*y(k1-p) + facm(i1+2)*y(k1+p)

      do j1= 3,n
        k1 = i1*p + j1
        z(k1) = facp(i1)*y(k1-p) + fac2(i1+1)*(y(k1-1)+y(k1+1)) + facm(i1+2)*y(k1+p) - y(k1)
      end do

      k1 = (i1+1)*p - 1
      z(k1) =  fac2(i1+1)*y(k1-1) - y(k1) + y(k1+1) + facp(i1)*y(k1-p) + facm(i1+2)*y(k1+p)

      k1 = (i1+1)*p
      z(k1) = fac2(i1+1)*y(k1-1) - y(k1)
      !print*,z(i1*(n+2)+1:(i1+1)*(n+2))


    end do

    !$OMP end parallel do

    !BCs
    !r=1s
    !i=n+1
    z(m-2*p+1)= -y(m-2*p+1) + fac2(n+1)*y(m-2*p+2) + fac_bc(1)*y(m-p+1)
    z(m-2*p+2) = facp(n)*y(m-3*p+2) + y(m-2*p+1) - y(m-2*p+2) + fac2(n+1)*y(m-2*p+3) +fac_bc(2)*y(m-p+2)

    z(m-2*p+3:m-p-2) = facp(n)*y(m-3*p+3:m-2*p-2) - y(m-2*n-1:m-n-4) + &
    fac2(n+1)*(y(m-2*p+2:m-p-3) + y(m-2*p+4:m-p-1)) + fac_bc(3:n)*y(m-p+3:m-2)

    z(m-p-1) = facp(n)*y(m-2*p-1) + fac2(n+1)*y(m-p-2) - y(m-p-1) + y(m-p) +fac_bc(p-1)*y(m-1)
    z(m-p) = fac2(n+1)*y(m-p-1) - y(m-p) + fac_bc(p)*y(m)

    !i=n+2
    z(m-p+1) = - y(m-p+1)
    z(m-p+2:m-1) = facp(n+1)*y(m-2*p+2:m-1-p) - y(m-p+2:m-1)
    z(m) = -y(m)

end subroutine mtvec



end module flow
