! This is a main program which can be used with the bmotion module
! if and as you wish as you develop your codes.
! It reads simulation parameters from a text file, data.in, which must be created.
!
! The subroutine simulate2_f90 is called, and output is written to the text files,
! x.txt,y.txt, and alpha_ave.txt which can be read in Python using np.loadtxt (see below)
!
! You should not submit this code with your assignment.
! To compile: gfortran -fopenmp -O3 -o main.exe p41_dev.f90 p4_main.f90
! To compile: gfortran -fopenmp -O3 -o main.exe p41_dev.f90 p4_main.f90
program p4_main
  use params
  use mpi
  implicit none
  integer :: n, numprocs, myid, s, e

  n =100
  numprocs=2
  myid=0

  call MPE_DECOMP1D(n, numprocs, myid, s, e)
  print*,s,e

end program p4_main
